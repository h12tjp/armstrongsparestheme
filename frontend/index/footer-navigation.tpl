{extends file="parent:frontend/index/footer-navigation.tpl"}

{block name="frontend_index_footer_column_newsletter"}
    <div class="footer--column column--newsletter is--last block">
        {block name="frontend_index_footer_column_newsletter_headline"}
            <div class="column--headline">About Us</div>
        {/block}

        {block name="frontend_index_footer_column_newsletter_content"}
            <div class="column--content">
                <p class="column--desc">
                    If you are looking for spares for any laundry appliance then Armstrong can help. We have a huge inventory of genuine spare parts for most commercial and industrial washing, drying and ironing systems.<br/><br/>© James Armstrong & Co Ltd 2018
                    Company No. 231369
                    Registered in England
                </p>

            </div>
        {/block}
    </div>

{/block}