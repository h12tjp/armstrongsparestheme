{extends file="parent:frontend/listing/product-box/box-basic.tpl"}

{* Product description *}
{block name='frontend_listing_box_article_description'}
    <div class="product--description">
        Part Number : {$sArticle.suppliernumber|strip_tags|truncate:240}

            <div class="product--delivery">
                <p class="delivery--information">

                    {if $sArticle.instock > 3}
                        <span class="delivery--text delivery--text-available">
                            <i class="delivery--status-icon delivery--status-available"></i>In Stock
                        </span>
                    {else}
                        <span class="delivery--text delivery--text-not-available">
                            <i class="delivery--status-icon delivery--status-not-available"></i>Out of Stock
                        </span>
                    {/if}
                </p>
            </div>


    </div>
{/block}