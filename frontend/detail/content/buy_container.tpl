{extends file="parent:frontend/detail/content/buy_container.tpl"}


{block name="frontend_detail_rich_snippets_weight"}
    {if $sArticle.weight}
        <meta itemprop="weight" content="{$sArticle.weight} kg"/>
    {/if}
{/block}

{* Product SKU *}
{block name='frontend_detail_data_ordernumber'}
    <li class="base-info--entry entry--sku">
        {* Product Weight - Label *}
        {block name='frontend_detail_data_ordernumber_label'}
            <strong class="entry--label">
                Weight
            </strong>
        {/block}

        {* Product Weight - Content *}
        {block name='frontend_detail_data_ordernumber_content'}
            <strong class="entry--content">
                {$sArticle.weight} {s name="detail/settings/weight_bw" namespace="backend/article/view/main"} {/s}
            </strong>
         {/block}
    </li>

    {* Need some help button*}
    {if $sInquiry}
    <li class="base-info--entry">
        <a href="{$sInquiry}" rel="nofollow" class="btn is--secondary is--full is--center">Need some help? Talk to an expert</a>
    </li>
    {/if}

{/block}
