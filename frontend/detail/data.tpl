{extends file="parent:frontend/detail/data.tpl"}

{* Regular price *}
{block name='frontend_detail_data_price_default'}
    <span class="price--content content--default">
        <meta itemprop="price" content="{$sArticle.price|replace:',':'.'}">
        {if $sArticle.priceStartingFrom}{s name='ListingBoxArticleStartsAt' namespace="frontend/listing/box_article"}{/s} {/if}
        {$sArticle.price|currency}
        {* Need new field setup for Nett price {$sArticle.nettprice|currency} *}
        {s name="Star" namespace="frontend/listing/box_article"}{/s}
    </span>
{/block}


{* Tax information *}
{block name='frontend_detail_data_tax'}
    <p class="product--tax" data-content="" data-modalbox="true" data-targetSelector="a" data-mode="ajax">
        {s name="DetailDataPriceInfo"}{/s}
    </p>
{/block}

