{extends file="parent:frontend/detail/tabs/description.tpl"}

{block name='frontend_detail_actions_contact'}
    {if $sInquiry}
        <li class="list--entry">
            <a href="{$sInquiry}" rel="nofollow" class="content--link link--contact" title="{"{s name='DetailLinkContact' namespace="frontend/detail/actions"}{/s}"|escape}">
                <i class="icon--arrow-right"></i> {s name="DetailLinkContact" namespace="frontend/detail/actions"}{/s}
            </a>
        </li>
    {/if}
{/block}